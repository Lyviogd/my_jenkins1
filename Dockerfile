FROM openjdk:8-jre-alpine



RUN mkdir /opt/app

WORKDIR /opt/app

COPY pom.xml ./pom.xml

#RUN adduser -h /var/lib -D jenks
#USER jenks 

ADD ./target/simple*.jar  ./simple-app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/opt/app/simple-app.jar"]
